-- ��������� ���� �����
CREATE DATABASE MilitaryWeaponsDB;
USE MilitaryWeaponsDB;
CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'YourPassword';
DROP INDEX cidx_WeaponID ON Weapons;
CREATE CLUSTERED INDEX cidx_WeaponID ON Weapons (WeaponID);
SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID('Weapons');

-- ³������� ����������� ����
IF EXISTS (SELECT * FROM sys.symmetric_keys WHERE name = 'MySymmetricKey')
    DROP SYMMETRIC KEY MySymmetricKey;
	
-- ��������� �������
CREATE TABLE Countries (
    CountryID INT PRIMARY KEY,
    CountryName VARCHAR(50) NOT NULL
);

CREATE TABLE Manufacturers (
    ManufacturerID INT PRIMARY KEY,
    ManufacturerName VARCHAR(50) NOT NULL
);

CREATE TABLE WeaponTypes (
    WeaponTypeID INT PRIMARY KEY,
    WeaponTypeName VARCHAR(50) NOT NULL
);

CREATE TABLE Weapons (
    WeaponID INT PRIMARY KEY,
    WeaponName VARCHAR(100),
    CountryID INT FOREIGN KEY REFERENCES Countries(CountryID),
    ManufacturerID INT FOREIGN KEY REFERENCES Manufacturers(ManufacturerID),
    WeaponTypeID INT FOREIGN KEY REFERENCES WeaponTypes(WeaponTypeID),
    YearOfManufacture INT,
    Caliber VARCHAR(20)
);

CREATE TABLE Users (
    UserID INT PRIMARY KEY,
    UserName VARCHAR(50),
    PasswordHash VARBINARY(MAX)
);

-- �������� �����
MERGE INTO Countries AS target
USING (VALUES 
    (1, 'USA'), 
    (2, 'Ukraine'), 
    (3, 'Poland'), 
    (4, 'China'), 
    (5, 'France')
) AS source (CountryID, CountryName)
ON target.CountryID = source.CountryID
WHEN MATCHED THEN
    UPDATE SET target.CountryID = source.CountryID
WHEN NOT MATCHED THEN
    INSERT (CountryID, CountryName) VALUES (source.CountryID, source.CountryName);

MERGE INTO Manufacturers AS target
USING (VALUES 
    (1, 'Lockheed Martin'), 
    (2, 'Boeing'), 
    (3, 'Kalashnikov Concern'), 
    (4, 'Kalashnikov Concern'), 
    (5, 'Rheinmetall AG')
) AS source (ManufacturerID, ManufacturerName)
ON target.ManufacturerID = source.ManufacturerID
WHEN MATCHED THEN
    UPDATE SET target.ManufacturerID = source.ManufacturerID
WHEN NOT MATCHED THEN
    INSERT (ManufacturerID, ManufacturerName) VALUES (source.ManufacturerID, source.ManufacturerName);

MERGE INTO WeaponTypes AS target
USING (VALUES 
    (1, 'Weapon'), 
    (2, 'Tanks and armored vehicles'), 
    (3, 'Aviation'), 
    (4, 'Artillery'), 
    (5, 'Missile weapons')
) AS source (WeaponTypeID, WeaponTypeName)
ON target.WeaponTypeID = source.WeaponTypeID
WHEN MATCHED THEN
    UPDATE SET target.WeaponTypeID = source.WeaponTypeID
WHEN NOT MATCHED THEN
    INSERT (WeaponTypeID, WeaponTypeName) VALUES (source.WeaponTypeID, source.WeaponTypeName);

MERGE INTO Weapons AS target
USING (VALUES 
    (1, 'AK-47', 1, 1, 1, 1947, '7.62'), 
    (2, 'M1 Abrams (tank)', 1, 2, 1, 1980, '105'), 
    (3, 'F-16 Fighting Falcon', 2, 1, 3, 1976, '20'), 
    (4, 'M777 Howitzer', 1, 2, 1, 2005, '155'), 
    (5, 'Tomahawk Cruise Missile', 1, 1, 1, 1980, '533')
) AS source (WeaponID, WeaponName, CountryID, ManufacturerID, WeaponTypeID, YearOfManufacture, Caliber)
ON target.WeaponID = source.WeaponID
WHEN MATCHED THEN
    UPDATE SET target.WeaponID = source.WeaponID
WHEN NOT MATCHED THEN
    INSERT (WeaponID, WeaponName, CountryID, ManufacturerID, WeaponTypeID, YearOfManufacture, Caliber) 
    VALUES (source.WeaponID, source.WeaponName, source.CountryID, source.ManufacturerID, source.WeaponTypeID, source.YearOfManufacture, source.Caliber);

MERGE INTO Users AS target
USING (VALUES 
    (1, 'admin1', HASHBYTES('SHA2_256', 'adminpassword')), 
    (2, 'admin2', HASHBYTES('SHA2_256', 'password123')),
    (4, 'admin3', HASHBYTES('SHA2_256', '87456')), 
    (3, 'admin4', HASHBYTES('SHA2_256', '78932')), 
    (5, 'admin5', HASHBYTES('SHA2_256', '10245'))
) AS source (UserID, UserName, PasswordHash)
ON target.UserID = source.UserID
WHEN MATCHED THEN
    UPDATE SET target.UserName = source.UserName, target.PasswordHash = source.PasswordHash
WHEN NOT MATCHED THEN
    INSERT (UserID, UserName, PasswordHash) VALUES (source.UserID, source.UserName, source.PasswordHash);

-- ��������� ��������� �����
ALTER TABLE Weapons
ADD CONSTRAINT FK_Manufacturer
FOREIGN KEY (ManufacturerID)
REFERENCES Manufacturers(ManufacturerID);

-- ��������� �������
-- ��������� ����������� ����������
CREATE STATISTICS stats_ManufacturerID ON Weapons (ManufacturerID);

-- ��������� �������
CREATE INDEX idx_YearOfManufacture ON Weapons (YearOfManufacture);

-- ��������� ��������� �������
CREATE INDEX idx_Country_Manufacturer ON Weapons (CountryID, ManufacturerID);

-- ��������� ����������� �������
CREATE UNIQUE INDEX idx_WeaponName ON Weapons (WeaponName);

-- ����� ���������������� �������
CREATE CLUSTERED INDEX cidx_WeaponID ON Weapons (WeaponID);

-- ����� ������������������ �������
CREATE NONCLUSTERED INDEX ncidx_Caliber ON Weapons (Caliber);

-- ��������� ��������
-- ��������� ����� �������� ��ﳿ
-- ������� ������ ��������� ���������, ���� ���� ����
IF OBJECT_ID('dbo.CreateFullBackup', 'P') IS NOT NULL
    DROP PROCEDURE dbo.CreateFullBackup;
GO

-- ³������� ��������� ���������
CREATE PROCEDURE dbo.CreateFullBackup
AS
BEGIN
    BACKUP DATABASE MilitaryWeaponsDB TO DISK = 'D:\Python';
END;

-- ��������� ���������� ���������
CREATE PROCEDURE dbo.CreateDifferentialBackup
AS
BEGIN
    BACKUP DATABASE MilitaryWeaponsDB TO DISK = 'D:\lab1.2' WITH DIFFERENTIAL;
END;

-- ��������� ���������� ��������� ������� ����������
CREATE PROCEDURE dbo.CreateTransactionLogBackup
AS
BEGIN
    BACKUP LOG MilitaryWeaponsDB TO DISK = 'D:\plitteck';
END;

-- ������� �������
CREATE PROCEDURE dbo.ClearTable
    @TableName NVARCHAR(50)
AS
BEGIN
    DECLARE @Query NVARCHAR(MAX);
    SET @Query = 'DELETE FROM ' + QUOTENAME(@TableName);
    EXEC sp_executesql @Query;
END;

-- �������������� �������
CREATE PROCEDURE dbo.DefragmentIndexes
AS
BEGIN
    DECLARE @TableName NVARCHAR(50);
    DECLARE TableCursor CURSOR FOR
    SELECT name
    FROM sys.tables;

    OPEN TableCursor;
    FETCH NEXT FROM TableCursor INTO @TableName;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        DECLARE @IndexName NVARCHAR(50);
        DECLARE IndexCursor CURSOR FOR
        SELECT name
        FROM sys.indexes
        WHERE object_id = OBJECT_ID(@TableName);

        OPEN IndexCursor;
        FETCH NEXT FROM IndexCursor INTO @IndexName;

        WHILE @@FETCH_STATUS = 0
        BEGIN
            DECLARE @RebuildQuery NVARCHAR(MAX);
            SET @RebuildQuery = 'ALTER INDEX ' + QUOTENAME(@IndexName) + ' ON ' + QUOTENAME(@TableName) + ' REBUILD';
            EXEC sp_executesql @RebuildQuery;

            FETCH NEXT FROM IndexCursor INTO @IndexName;
        END

        CLOSE IndexCursor;
        DEALLOCATE IndexCursor;

        FETCH NEXT FROM TableCursor INTO @TableName;
    END

    CLOSE TableCursor;
    DEALLOCATE TableCursor;
END;

-- ��������� ����� �������
-- ��������� ��'� �����
CREATE LOGIN MilitaryWeaponsUser WITH PASSWORD = 'userpassword';

-- ��������� �����������
CREATE USER MilitaryWeaponsUser FOR LOGIN MilitaryWeaponsUser;

-- ��������� �����
CREATE ROLE director;
CREATE ROLE deputy;
CREATE ROLE captain;
CREATE ROLE sen_manager;
CREATE ROLE jun_manager;
CREATE ROLE sen_employee;
CREATE ROLE jun_employee;

-- ������� ��������� ������������ �� �����
ALTER ROLE director ADD MEMBER MilitaryWeaponsUser;
ALTER ROLE deputy ADD MEMBER MilitaryWeaponsUser;
ALTER ROLE captain ADD MEMBER MilitaryWeaponsUser;
ALTER ROLE sen_manager ADD MEMBER MilitaryWeaponsUser;
ALTER ROLE jun_manager ADD MEMBER MilitaryWeaponsUser;
ALTER ROLE sen_employee ADD MEMBER MilitaryWeaponsUser;
ALTER ROLE jun_employee ADD MEMBER MilitaryWeaponsUser;

-- ������� ���� ��� director
GRANT SELECT, INSERT, UPDATE, DELETE ON Weapons TO director;

-- ������� ���� ��� deputy
GRANT SELECT, INSERT, UPDATE ON Weapons TO deputy;

-- ������� ���� ��� captain
GRANT SELECT, UPDATE ON Weapons TO captain;

-- ������� ���� ��� sen_manager
GRANT SELECT ON Weapons TO sen_manager;

-- ������� ���� ��� jun_manager
GRANT SELECT, INSERT ON Weapons TO jun_manager;

-- ������� ���� ��� sen_employee
GRANT SELECT ON Weapons TO sen_employee;

-- ������� ���� ��� jun_employee
GRANT SELECT, INSERT ON Weapons TO jun_employee;

-- ����������
CREATE ASYMMETRIC KEY ASymKey1
WITH ALGORITHM = RSA_2048
ENCRYPTION BY PASSWORD = '1111';
-- ��������� ������������� ������
INSERT INTO Weapons (WeaponID, WeaponName, CountryID, ManufacturerID, WeaponTypeID, YearOfManufacture, Caliber)
VALUES (
    6,
    ENCRYPTBYASYMKEY(ASYMKEY_ID('ASymKey1'), CONVERT(VARBINARY(MAX), 'AK-74')),
    1,
    3,
    1,
    1974,
    '5.45mm'
);


-- ��������� �������������� ����
SELECT
WeaponID,
CONVERT(NVARCHAR(500), DECRYPTBYASYMKEY(ASYMKEY_ID('ASymKey1'), WeaponName, CONVERT(NVARCHAR(10), '1111'))) AS WeaponName,
CountryID,
ManufacturerID,
WeaponTypeID,
YearOfManufacture,
Caliber
FROM Weapons;

-- ��������� ��������� ����� ����������
-- Drop the symmetric key
DROP SYMMETRIC KEY MySymmetricKey;

-- Drop the certificate
DROP CERTIFICATE MyCertificate;

-- Drop the master key
DROP MASTER KEY;

CREATE MASTER KEY ENCRYPTION BY PASSWORD = '1111';

-- �������� ��������� ����� ����������
-- �������� ������������ �������
-- ��������� ���� ��������� ����
DECLARE @BackupPath NVARCHAR(4000);

EXEC master.dbo.xp_instance_regread
    N'HKEY_LOCAL_MACHINE',
    N'Software\Microsoft\MSSQLServer\MSSQLServer',
    N'BackupDirectory',
    @BackupPath OUTPUT;

SELECT @BackupPath AS BackupPath;

BACKUP MASTER KEY TO FILE = 'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER02\MSSQL\Backup\MasterKeyBackup'
ENCRYPTION BY PASSWORD = 'YourPassword';

-- ��������� �����������
CREATE CERTIFICATE MyServerCert WITH SUBJECT = 'My Certificate';

-- �������� ��������� �����������
BACKUP CERTIFICATE MyServerCert TO FILE = 'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER02\MSSQL\Backup\cert_backup.cer';

-- ��������� ����� ���������� ��� �� �� ���� ���������
CREATE SYMMETRIC KEY MySymmetricKey
WITH ALGORITHM = AES_256
ENCRYPTION BY CERTIFICATE MyServerCert;

-- ��������� �����
OPEN SYMMETRIC KEY MySymmetricKey DECRYPTION BY CERTIFICATE MyServerCert;


-- ���������
SELECT * FROM Countries;
SELECT * FROM Manufacturers;
SELECT * FROM WeaponTypes;
SELECT
    WeaponName,
    CountryName,
    ManufacturerName,
    WeaponTypeName,
    YearOfManufacture,
    Caliber
FROM Weapons
JOIN Countries ON Weapons.CountryID = Countries.CountryID
JOIN Manufacturers ON Weapons.ManufacturerID = Manufacturers.ManufacturerID
JOIN WeaponTypes ON Weapons.WeaponTypeID = WeaponTypes.WeaponTypeID;

SELECT * FROM Users;
OPEN SYMMETRIC KEY MySymmetricKey
DECRYPTION BY CERTIFICATE MyServerCert;


SELECT
    WeaponName,
    CONVERT(VARCHAR(MAX), DECRYPTBYKEY(WeaponName)) AS DecryptedWeaponName
FROM Weapons;

